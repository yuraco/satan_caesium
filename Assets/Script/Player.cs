﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    private Animator anim;
    private Vector3 MoveVec;
    public float Gravity = 20.0f;
    public int Hp;
    public GameObject Maincamera;
    private CharacterController controller;
	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();	
	}
	
	// Update is called once per frame
	void Update () 
    {
        //コントローラーのスティックを倒した方向をとる
        float axisValueX = Input.GetAxis("Horizontal");
        float axisValueZ = Input.GetAxis("Vertical");
        //プレイヤーの移動ベクトルを制作
        MoveVec = (Maincamera.transform.right * Input.GetAxis("Horizontal")) + (Maincamera.transform.forward * Input.GetAxis("Vertical"));
        MoveVec.y = 0;
        //スティックを倒しているときに、倒した方向にプレイヤーが向くようにローテーションを設定
        if (System.Math.Abs(axisValueX) >= 0.1f || System.Math.Abs(axisValueZ) >= 0.1f)
        {
            transform.rotation = Quaternion.LookRotation(MoveVec);
        }

        MoveVec.y -= Gravity * Time.deltaTime;
        controller.Move(MoveVec / 15.0f);
        //アニメーションのセット、マイナスの値だとアニメーションしないので絶対値で
        anim.SetFloat("Speed", (System.Math.Abs(axisValueX) + System.Math.Abs(axisValueZ)) / 2.0f);
	}
	
}
