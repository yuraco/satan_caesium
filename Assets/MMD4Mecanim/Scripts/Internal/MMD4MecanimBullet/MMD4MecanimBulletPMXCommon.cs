﻿using UnityEngine;
using System.Collections;
using BulletXNA;
using BulletXNA.BulletCollision;
using BulletXNA.BulletDynamics;
using BulletXNA.LinearMath;

public class MMD4MecanimBulletPMXCommon
{
	public enum PMXFileType
	{
		None,
		PMD,
		PMX,
	}
	
	public enum PMDBoneType
	{
		Rotate,
		RotateAndMove,
		IKDestination,
		Unknown,
		UnderIK,
		UnderRotate,
		IKTarget,
		NoDisp,
		Twist,
		FollowRotate,
	}
	
	public enum PMXShapeType
	{
		Sphere,
		Box,
		Capsule,
	}
	
	public enum PMXRigidBodyType
	{
		Kinematics,
		Simulated,
		SimulatedAligned,
	}
}
